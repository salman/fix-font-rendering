#!/bin/sh

DATE=`date +%Y-%m-%d.%H.%M.%S`

# raname the existing file
if [ -f ~/.config/fontconfig/fonts.conf ]; then
    cp ~/.config/fontconfig/fonts.conf ~/.config/fontconfig/fonts.conf.$DATE
fi

wget https://framagit.org/snippets/368/raw -O ~/.config/fontconfig/fonts.conf
